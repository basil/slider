enum ActiveTrack {
	START,
	LOOP
}

const playSliderTheme = async (): Promise<() => void> => {
	// @ts-ignore Super lazy and basic preloading
	const start = await loadAudio(window.sliderStart as ArrayBuffer);
	// @ts-ignore
	const loop = await loadAudio(window.sliderLoop as ArrayBuffer);
	loop.source.loop = true;

	let activeTrack = ActiveTrack.START;

	const onStartEnd = () => {
		loop.source.start();
		activeTrack = ActiveTrack.LOOP;
	};

	start.source.start();
	start.source.addEventListener('ended', onStartEnd);

	const fadeOut = () => {
		// In seconds
		const fadeOutDuration = 1;

		if (activeTrack === ActiveTrack.START) {
			// In seconds
			const timeUntilLoop = start.source.buffer!.duration! - start.source.context.currentTime;

			// If we're still on the start track and the fadeout wouldn't happen in time, we have to use different logic
			// to ensure that the fadeout still happens smoothly
			if (timeUntilLoop < fadeOutDuration) {
				console.log('this!', timeUntilLoop);
				start.gainNode.gain.setValueCurveAtTime([1, 1 - timeUntilLoop], start.source.context.currentTime, timeUntilLoop);
				loop.gainNode.gain.setValueCurveAtTime(
					// Start the loop at the last value
					[1 - timeUntilLoop, 0],
					// According to MDN: "A double representing the time (in seconds) after the AudioContext was first created"
					// Hence, we need to add `currentTime`
					loop.source.context.currentTime + timeUntilLoop,
					fadeOutDuration - timeUntilLoop
				);
			} else {
				start.gainNode.gain.setValueCurveAtTime([1, 0], start.source.context.currentTime, fadeOutDuration);
			}
		} else if (activeTrack === ActiveTrack.LOOP) {
			loop.gainNode.gain.setValueCurveAtTime([1, 0], loop.source.context.currentTime, fadeOutDuration);
		}

		setTimeout(() => {
			start.source.removeEventListener('ended', onStartEnd);

			// I couldn't find a way to deterine the playback state, so this somewhat hacky solution will do
			try { start.source.stop(); } catch {}
			try { loop.source.stop(); } catch {}
		}, fadeOutDuration * 1000);
	};

	return fadeOut;
};

// Fetches audio file as ArrayBuffer
async function getAudioBuffer(path: string) {
	const res = await fetch(path);
	return await res.arrayBuffer();
}

// Adapted from mon's SyncLoop (https://github.com/mon/SyncLoop)
const loadAudio = async (arrayBuffer: ArrayBuffer): Promise<{ source: AudioBufferSourceNode; gainNode: GainNode; }> => {
	// Create context & gain
	const context = new AudioContext();
	const gainNode = context.createGain();
	gainNode.connect(context.destination);

	// Create empty buffer
	const emptyBuffer = context.createBuffer(1, 1, 22050);
	const emptySource = context.createBufferSource();
	emptySource.buffer = emptyBuffer;

	// Connect to output (your speakers)
	emptySource.connect(context.destination);

	// Load audio
	const buffer = await context.decodeAudioData(arrayBuffer);
	const source = context.createBufferSource();
	source.buffer = buffer;
	source.connect(gainNode);

	return { source, gainNode };
};

export default playSliderTheme;
export { getAudioBuffer };
