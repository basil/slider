import { For, Show, createEffect, createSignal } from 'solid-js';
import Toggle from './Toggle';
import slider, { preloadSliderAudio } from '../ts/slider';

const genCookieList = (list: string) => list
	.trim().split('\n')
	.map(t => t + ' Cookies')
	.sort(() => Math.random() - 0.5);

const seriousList = genCookieList(`
Performance
Strictly Necessary
Session (Analytics)
Session (Authentication)
Tracking
Analytics
Preference
Third-party
First-party
Functionality
Statistics
Persistent
Session
`);

const funList = genCookieList(`
Fun
Secret
Not-so-secret
Totally Legal
Union Prevention
Crypto Miner
Adware
Space-wasting
Oil Drilling
Paid
Unicorn
Mysterious
Functionally Lacking
`);

const list = [...seriousList, ...funList];

const Popup = () => {
	createEffect(() => {
		preloadSliderAudio();
	});

	const [open, setOpen] = createSignal(true);
	const [declined, setDeclined] = createSignal(false);
	const [fadeout, setFadeout] = createSignal<() => void>();

	return (
		<Show when={open()}>
			<div class='absolute bottom-4 left-4 w-96 max-h-96 overflow-auto bg-slate-100 rounded-md p-2 shadow-md shadow-slate-700/10 border-[1px] border-slate-200'>
				<Show
					when={!declined()}
					fallback={<div class='flex flex-col gap-2 p-2'>
						<For each={list}>
							{item => <div class='flex gap-2 items-center'>
								<Toggle />
								{item}
							</div>}
						</For>

						<button
							class='text-sm font-semibold bg-green-600 hover:bg-green-700 rounded px-2 py-1 mt-2 text-white transition-colors'
							onClick={() => {
								const fadeoutFn = fadeout();
								if (fadeoutFn) fadeoutFn();
								setOpen(false);
							}}
						>
							Close
						</button>
					</div>}
				>
					<h1 class='text-slate-900 font-bold text-2xl mb-1'>Accept cookies?</h1>
					<p class='text-slate-500 mb-6 leading-5'>In order to continue using the page, we need you to accept our cookies.</p>

					<div class='flex flex-row gap-2.5 [&>*]:transition-colors'>
						<button
							class='text-sm font-semibold bg-green-600 hover:bg-green-700 rounded px-2 py-0.5 text-white'
							onClick={() => setOpen(false)}
						>
							Accept
						</button>
						<button 
							class='text-sm text-slate-400 hover:text-slate-300 my-0.5'
							onClick={async () => {
								setDeclined(true)
								const fadeout = await slider();
								setFadeout(() => fadeout);
							}}
						>
							Decline
						</button>
					</div>
				</Show>
			</div>
		</Show>
	);
};

export default Popup;
