import { createSignal } from 'solid-js';

const randInt = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1) + min);

const Toggle = () => {
	const [time, setTime] = createSignal<number|null>(null);

	let el: HTMLInputElement | undefined;

	return <input
		type='checkbox' checked={true}
		ref={el}
		class='relative bg-slate-300 hover:bg-slate-400 checked:bg-green-600 hover:checked:bg-green-700 appearance-none h-6 w-12 rounded-full transition-colors cursor-pointer    after:absolute after:top-0.5 after:transition-all after:ease-in-out checked:after:left-[1.625rem] after:left-0.5 after:bg-white after:rounded-full after:h-5 after:w-5'
		onClick={() => {
			const previousTimer = time();
			if (previousTimer) {
				clearTimeout(previousTimer);
				setTime(null);
			}

			const timeout = setTimeout(() => {
				if (el) el.checked = true;
			}, randInt(2500, 5000));

			setTime(timeout);
		}}
	/>;
};

export default Toggle;
