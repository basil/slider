import n64 from '../../static/n64.svg?raw';
import duration from '../duration';

const showSliderLogo = () => {
	addCss(`
		@keyframes logoAppear {
			from {
				opacity: 0;
				scale: 1.5;
			}
			40% { opacity: 1; }

			to {
				opacity: 0;
				scale: 0.8;
			}
		}

		.sliderLogo {
			position: absolute;
			translate: -50% -50%;
			left: 50%; top: 50%;
			height: 120px; width: 120px;

			animation: logoAppear ${duration}s linear;
			animation-fill-mode: forwards;
		}
	`);

	// Maybe don't allow for triggering multiple times?
	const parser = new DOMParser();
	const logoDoc = parser.parseFromString(n64, 'image/svg+xml');
	const svg = logoDoc.documentElement;

	svg.classList.add('sliderLogo');
	document.body.append(svg);

	// Remove the SVG after it has faded out
	setTimeout(() => document.body.removeChild(svg), duration * 1000);
};

const addCss = (css: string) => {
	// Don't duplicate styles
	if (document.querySelector('#sliderStyles')) return;

	const styleSheet = document.createElement('style');
	styleSheet.id = 'sliderStyles';
	styleSheet.innerText = css;
	document.head.appendChild(styleSheet);
};

export default showSliderLogo;
