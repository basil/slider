# Slider Gag

Feel free to use the Slider script in your own projects :) It's detached from the rest of the app (which just serves as a demo), so you should be able to just copy the audio files, SVG, and scripts to another project with some fiddling (pun intended).
