import showSliderLogo from './logo.ts';
import playSliderTheme, { getAudioBuffer } from './music.ts';

const slider = async (): Promise<() => void> => {
	showSliderLogo();

	// Just in case it hasn't been already
	preloadSliderAudio();
	const fadeOut = await playSliderTheme();
	return fadeOut;
};

const preloadSliderAudio = async () => {
	// @ts-ignore
	window.sliderStart = await getAudioBuffer('/slider-start.mp3');
	// @ts-ignore
	window.sliderLoop = await getAudioBuffer('/slider-loop.mp3');
}

export default slider;
export { preloadSliderAudio };
